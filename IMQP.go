package casino

import (
	"fmt"
	"net/url"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
)

//操作类型
const (
	Login       = "1"
	Profile     = "2"
	TransferIn  = "3"
	TransferOut = "4"
)

const IMQP string = "IMQP"

func imqpPack(md5Key, desStr, desKey string, args map[string]string) string {

	md5Str := args["agentId"] + args["timestamp"] + md5Key
	p := url.Values{}

	p.Set("sign", getMD5Hash(md5Str))
	desRes, _ := desEncrypt([]byte(desStr), []byte(desKey))
	p.Set("param", desRes)

	for k, v := range args {
		p.Set(k, v)
	}
	return p.Encode()
}

func imqpReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	lang := map[string]string{
		"1": "zh-CN",
	}

	args := map[string]string{
		"agentId":   param["agent_id"].(string),
		"timestamp": param["s"].(string),
	}
	desData := url.Values{}
	desData.Set("ac", Login)
	desData.Set("userCode", param["username"].(string))
	desData.Set("ip", param["ip"].(string))
	desData.Set("gameId", "0")
	desData.Set("lang", lang[param["lang"].(string)])

	str := imqpPack(param["md5_key"].(string), desData.Encode(), param["des_key"].(string), args)
	reqUrl := fmt.Sprintf("%s/agentHandle?%s", param["api"].(string), str)

	statusCode, body, err := httpGetWithPushLog(zlog, IMQP, reqUrl)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	vv := v.Get("data")

	if vv.GetInt("code") != 0 {
		return Failure, fmt.Sprintf("%d", vv.GetInt("code"))
	}

	return Success, string(vv.GetStringBytes("fullUrl"))
}

func imqpLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {
	return imqpReg(zlog, param)
}

func imqpBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {
	var p fastjson.Parser

	args := map[string]string{
		"agentId":   param["agent_id"].(string),
		"timestamp": param["s"].(string),
	}
	desData := url.Values{}
	desData.Set("ac", Profile)
	desData.Set("userCode", param["username"].(string))

	str := imqpPack(param["md5_key"].(string), desData.Encode(), param["des_key"].(string), args)
	reqUrl := fmt.Sprintf("%s/agentHandle?%s", param["api"].(string), str)
	statusCode, body, err := httpGetWithPushLog(zlog, IMQP, reqUrl)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	vv := v.Get("data")

	if vv.GetInt("code") != 0 {
		return Failure, fmt.Sprintf("%d", vv.GetInt("code"))
	}

	balance, _ := GetBalanceFromByte(vv.GetStringBytes("freeMoneyStr"))
	return Success, balance
}

func imqpTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	transferType := TransferIn
	if param["type"].(string) == "out" {
		transferType = TransferOut
	}

	args := map[string]string{
		"agentId":   param["agent_id"].(string),
		"timestamp": param["s"].(string),
	}
	desData := url.Values{}
	desData.Set("ac", transferType)
	desData.Set("userCode", param["username"].(string))
	desData.Set("money", param["amount"].(string))
	desData.Set("orderId", param["id"].(string))

	str := imqpPack(param["md5_key"].(string), desData.Encode(), param["des_key"].(string), args)
	reqUrl := fmt.Sprintf("%s/agentHandle?%s", param["api"].(string), str)
	statusCode, body, err := httpGetWithPushLog(zlog, IMQP, reqUrl)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	vv := v.Get("data")

	if vv.GetInt("code") != 0 {
		return Failure, fmt.Sprintf("%d", vv.GetInt("code"))
	}

	return Success, param["id"].(string)
}

//转账确认
func imqpConfirm(zlog *fluent.Fluent, param map[string]interface{}) bool {

	return false
}
